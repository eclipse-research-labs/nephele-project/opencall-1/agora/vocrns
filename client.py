#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import asyncio
import logging
import time

from wotpy.wot.servient import Servient
from wotpy.wot.wot import WoT

logging.basicConfig()
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


async def main():
    wot = WoT(servient=Servient())
    # consumed_thing = await wot.consume_from_url('http://localhost:8080/crns-sensor')
    # consumed_thing = await wot.consume_from_url('https://crns.vaimee.com:9090/crns-sensor') 
    consumed_thing = await wot.consume_from_url('http://127.0.0.1:9090/crns-sensor')

    LOGGER.info('#### Consumed Thing: {}'.format(consumed_thing))

    statusmqtt = await consumed_thing.read_property('mqttStatus')
    LOGGER.info('MQTT STATUS: {}'.format(statusmqtt))

    day = await consumed_thing.read_property('day')
    LOGGER.info('test value None, value is: {}'.format(day))

    statusmqtt = await consumed_thing.invoke_action('mqttConnect')
    LOGGER.info('MQTT CONNECT: {}'.format(statusmqtt))


    LOGGER.info('#### Start mqtt subscrber...')
    statusmqtt = await consumed_thing.read_property('mqttStatus')
    LOGGER.info('MQTT STATUS: {}'.format(statusmqtt))

    LOGGER.info('#### Sleep 5s')
    time.sleep(5)

    statusmqtt = await consumed_thing.read_property('mqttStatus')
    LOGGER.info('MQTT STATUS: {}'.format(statusmqtt))

    LOGGER.info('#### Sleep 80s')
    time.sleep(80)

    LOGGER.info('#### Readings all last values')

    day = await consumed_thing.read_property('day')
    LOGGER.info('day value is: {}'.format(day))

    _time = await consumed_thing.read_property('time')
    LOGGER.info('time value is: {}'.format(_time))

    neutrons = await consumed_thing.read_property('neutrons')
    LOGGER.info('neutrons value is: {}'.format(neutrons))

    muons = await consumed_thing.read_property('muons')
    LOGGER.info('muons value is: {}'.format(muons))

    gamma = await consumed_thing.read_property('gamma')
    LOGGER.info('gamma value is: {}'.format(gamma))

    integration_time = await consumed_thing.read_property('integration_time')
    LOGGER.info('integration_time value is: {}'.format(integration_time))

    v_in = await consumed_thing.read_property('v_in')
    LOGGER.info('v_in value is: {}'.format(v_in))

    temperature_in = await consumed_thing.read_property('temperature_in')
    LOGGER.info('temperature_in value is: {}'.format(temperature_in))

    temperature_ext = await consumed_thing.read_property('temperature_ext')
    LOGGER.info('temperature_ext value is: {}'.format(temperature_ext))

    ur = await consumed_thing.read_property('ur')
    LOGGER.info('ur value is: {}'.format(ur))

    pressure = await consumed_thing.read_property('pressure')
    LOGGER.info('pressure value is: {}'.format(pressure))

    # statusmqtt = await consumed_thing.invoke_action('mqttDisconnect')
    # LOGGER.info('MQTT DISCONNECT: {}'.format(statusmqtt))

    # while statusmqtt:
    #     LOGGER.info('#### Sleep 5s')
    #     time.sleep(5)
    #     statusmqtt = await consumed_thing.read_property('mqttStatus')
    #     LOGGER.info('MQTT STATUS: {}'.format(statusmqtt))
    
    LOGGER.info('#### Test ends')

if __name__ == '__main__':
    loop = asyncio.run(main())
