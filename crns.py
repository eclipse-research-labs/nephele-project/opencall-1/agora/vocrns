#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
WoT application to expose a Thing that provides the values of CRNS Sensor
"""
from dotenv import load_dotenv
import os
import asyncio
import json
from wotpy.wot.exposed.thing import ExposedThing
from wotpy.wot.servient import Servient
from amqtt.client import MQTTClient, ClientException
from amqtt.mqtt.constants import QOS_0, QOS_1, QOS_2

### LOGGING
import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
amqtt_logger = logging.getLogger("amqtt")
amqtt_logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
amqtt_logger.addHandler(handler)

### MQTT 
load_dotenv()
MQTT_TOPIC = os.environ.get('MQTT_TOPIC')
MQTT_HOST= os.environ.get('MQTT_HOST')
logging.info(f"connectting to: {MQTT_HOST}")
logging.info(f"subscribing to: {MQTT_TOPIC}")
mqttRun=False
mqttStatus_init=False
### DATA
day_init=None
time_init=None
neutrons_init=None
muons_init=None
gamma_init=None
integration_time_init=None
v_in_init=None
temperature_in_init=None
temperature_ext_init=None
ur_init=None
pressure_init=None

async def mqtt_subscriber():
    global mqttRun
    mqttRun=True
    client = MQTTClient()
    await client.connect(MQTT_HOST,True)   
    await client.subscribe([
        (MQTT_TOPIC, QOS_0)
    ])
    try:
        while mqttRun:
            message = await client.deliver_message()
            packet = message.publish_packet
            msg = packet.payload.data.decode('utf-8')
            print(f"recived msg: {msg}")
            json_object = json.loads(msg)
            await exposed_thing.properties['day'].write(json_object["day"])
            await exposed_thing.properties['time'].write(json_object["time"])
            await exposed_thing.properties['neutrons'].write(json_object["neutrons"])
            await exposed_thing.properties['muons'].write(json_object["muons"])
            await exposed_thing.properties['gamma'].write(json_object["gamma"])
            await exposed_thing.properties['integration_time'].write(json_object["integration_time"])
            await exposed_thing.properties['v_in'].write(json_object["v_in"])
            await exposed_thing.properties['temperature_in'].write(json_object["temperature_in"])
            await exposed_thing.properties['temperature_ext'].write(json_object["temperature_ext"])
            await exposed_thing.properties['ur'].write(json_object["ur"])
            await exposed_thing.properties['pressure'].write(json_object["pressure"])
    except ClientException as ce:
        print(f"Client exception: {ce}")
    finally:
        await client.disconnect()
        await exposed_thing.properties['mqttStatus'].write(False)

async def on_mqtt_message(payload):
        print(f"Messaggio MQTT ricevuto: {payload}")


async def mqttConnect_handler(params):
    if not await exposed_thing.read_property('mqttStatus'):
        #  mqtt_subscriber is async but we will not await it on purpose
        await exposed_thing.properties['mqttStatus'].write(True)
        asyncio.create_task(mqtt_subscriber())
        current_connect_status = await exposed_thing.read_property('mqttStatus')
        print(f"subscribing status: {current_connect_status}")
        # await crnsrawSubcribe()
    return current_connect_status

async def mqttDisconnect_handler(params):
    global mqttRun
    mqttRun=False
    return await exposed_thing.read_property('mqttStatus')