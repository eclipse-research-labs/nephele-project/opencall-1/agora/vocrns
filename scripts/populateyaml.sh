#!/bin/sh
echo "Setting vo inlfaxDB yaml credential"
echo "" >> /app/vo_crns.yaml
echo "    address: ${influx_address}" >> /app/vo_crns.yaml
echo "    dbUser: ${influx_dbUser}" >> /app/vo_crns.yaml
echo "    dbPass: ${influx_dbPass}" >> /app/vo_crns.yaml
echo "    dbToken: ${influx_dbToken}" >> /app/vo_crns.yaml
cat /app/vo_crns.yaml
echo "Start Vo"
vo-wot -t crns_td.json -f vo_crns.yaml crns.py