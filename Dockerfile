FROM python:3.11-slim
WORKDIR /app

RUN pip install vo-wot
RUN pip install python-dotenv

COPY crns.py /app/crns.py
COPY crns_td.json /app/crns_td.json
COPY vo_crns_draft.yaml /app/vo_crns_draft.yaml
#COPY scripts/populateyaml.sh /app/populateyaml.sh
#RUN chmod +x /app/populateyaml.sh

# Copy configurator
COPY populate_yaml.py /app/populate_yaml.py
COPY docker_cmd.sh /app/docker_cmd.sh
RUN chmod +x /app/docker_cmd.sh

CMD ["/app/docker_cmd.sh"]